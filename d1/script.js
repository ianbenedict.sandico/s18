// let names = [	
// 				['kevin','joey','noel'],
// 				['joshua','arick','john'],
// 				['aly','ian','therese']
// 			];




// let names = ['therese' , 'joshua', 'ian', 'kevin'];

// for (let i = 0; i < names.length; i++){
// 	console.log(names[i]);
// }

// console.log(names[0][1]);

// key concept
// key loop forEach
// kevin, joey, noel
// joshua, arick, john
// aly, ian, therese

// let names = [
// 				['kevin','joey','noel'],
// 				['joshua','arick','noel'],
// 				['aly','ian','therese']			
// 			];
// for (let i = 0; i < names.length  ; i++){
// 	for(let j = 0; j < names[i].length; j++){ 
// 	console.log(names[i][j]);
// 	}	// console.log(names[i])
// }

// let nums = ['a','b','c'];

// for(let i = 0; i < nums.length; i++){
// 	console.log(nums[i]);
// 	//
// }

/*
nums[0]= a
nums[1]= b
nums[2]= c

*/

// let nums = [	
// 				['kevin','joey','noel'],
// 				['joshua','arick','john'],
// 				['aly','ian','therese']
// 			];

// for(let i = 0; i < nums.length; i++){
// 	//console.log(nums[i]);
// 	for(let j = 0; j < nums[i].length; j++){
// 		console.log(nums[i][j]);

// 	}

// }

// nums[0]=  ['kevin','joey','noel'],
// nums[1]=  ['joshua','arick','john'],
// nums[2]=  ['aly','ian','therese']

/*
let num =2
num += 3

*/

// Arrays

// let grades = [98,87,85];

// let myInfo = [30, 'new york', ]

//js object
//key-value pairs
// let myGrades = {
// 					english: 98, 
// 					math: 87, 
// 					science:95
// 				}
// console.log(myGrades.english)

//to access a value in an property, you need to use . notation
//person = object, property= lastname, firstname, value= doe john japan

let person ={
	lastName: 'Doe',
	firstName: 'John',
	math:97,
	eng:89,
	sci:95,
	location:{
		city: 'Tokyo',
		country: 'Japan'
	},
	emails:['john@mail.com', 'jd@gmail.com'],
	sayHi: function(){
		console.log('hi guys!')
	},
	displayName:function(){
		return this.lastName + "," + this.firstName;
	},
	giveAverage: function(){
		return (this.math + this.eng + this.sci)/3;
	}
}

// console.log(person.location.country)
// console.log(person.emails)

//method, when function is inside an object.
// person.sayHi()
// console.log(person.displayName())

let ave = person.giveAverage() //94
console.log('The average is ' + ave)

function passOrFail(num1){
	if(num1>=75){
		alert('Passed')
	}else{
		alert('Fail')
	}
}

passOrFail(ave)


// person.displayName()

// let movie = {
// 	title: 'Pulp Fiction',
// 	genre: 'actions',
// 	director: 'Quentin Tarantino',
// 	rating: 5,
// 	length: 120,
// 	comments: [
// 				{
// 					name: "Stan Lee",
// 					comment: "Great!",
// 					reply:[{},{},{}]
// 				},
// 				{
// 					name:"Stanle Kubrick",
// 					comment:"Awesome film"
// 				}
// 			]
// }

// console.log(movie.comments[0].name);