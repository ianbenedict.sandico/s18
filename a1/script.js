let movie = [
	{
		title: "Jaws",
		genre: "Thriller",
		releaseDate: "8/27/2021",
		rating: 6.5,
		displayRating: function(){
			return "The movie " + this.title +" has " + this.rating + " stars."
		}
	},
	{
		title: "Spider-Man",
		genre: "Action",
		releaseDate: "7/23/2020",
		rating: 8.0,
		displayRating: function(){
			return "The movie " + this.title +" has " + this.rating + " stars."
		}
	},
	{
		title: "Lord of the ring",
		genre: "Adventure",
		releaseDate: "3/6/2000",
		rating: 8.5,
		displayRating: function(){
			return "The movie " + this.title +" has " + this.rating + " stars."
		}
	},
	{
		title: "Crazy Grandpa",
		genre: "Comedy",
		releaseDate: "2/5/2015",
		rating: 9.5,
		displayRating: function(){
			return "The movie " + this.title +" has " + this.rating + " stars."
		}
	},
	{
		title: "Gagamboy",
		genre: "Drama",
		releaseDate: "3/15/1996",
		rating: 2,
		displayRating: function(){
			return "The movie " + this.title +" has " + this.rating + " stars."
		}
	}
	]

function showAllMovies(){
	for (let i = 0; i < movie.length; i++){
		if (movie[i].genre == "Thriller" || movie[i].genre == "Comedy" || movie[i].genre == "Drama"){
			console.log(movie[i].title + " is a " + movie[i].genre + " movie ")
		}else{
			console.log(movie[i].title + " is an " + movie[i].genre + " movie ");
		}
	}
}

showAllMovies()

// let movie1 = {
// 		title: "Jaws",
// 		genre: "Thriller",
// 		releaseDate: "8/27/2021",
// 		rating: 6.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
// let movie2 = {
// 		title: "Spider-Man",
// 		genre: "Action",
// 		releaseDate: "7/23/2020",
// 		rating: 8.0,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
// let movie3 = {	
// 		title: "Lord of the ring",
// 		genre: "Adventure",
// 		releaseDate: "3/6/2000",
// 		rating: 8.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
// let movie4 = {	
// 		title: "Crazy Grandpa",
// 		genre: "Comedy",
// 		releaseDate: "2/5/2015",
// 		rating: 9.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
// let movie5 = {	
// 		title: "Gagamboy",
// 		genre: "Drama",
// 		releaseDate: "3/15/1996",
// 		rating: 2,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
	
// function showAllMovies(){
// 	if (movie1.genre == "Thriller" || movie1.genre == "Comedy" || movie1.genre == "Drama" || 
// 		movie2.genre == "Thriller" || movie2.genre == "Comedy" || movie2.genre == "Drama"){
// 		console.log(movie[i].title + " is a " + movie[i].genre + " movie ")
// 	}else{
// 		console.log(movie[i].title + " is an " + movie[i].genre + " movie ");
// 	}
// }